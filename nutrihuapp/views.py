from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
import json
import urllib2
from django.http import HttpResponse, HttpResponseRedirect
from .models import Person
from django.contrib.auth import authenticate, login
from samman import UsernameValidated, PasswordValidated, EmailValidated, bmi, bmr, tdee, met, color, CreateCache
from twitter import get_all_words, not_symbol, get_all_nouns, get_highest_valued_keys, main, get_youtube_links

google_apiKey = 'AIzaSyBkOwZ54lszh7W6hm_jKx_ehuTigL1lIN8'
google_client_id = '364020523774-gsvrr7ov6mtphn8iav2ort26n3js36l0.apps.googleusercontent.com'
nutritionix_api = 'https://api.nutritionix.com/v1_1/search/'
nutritionix_api_fields = 'fields=item_name,brand_name,item_id,nf_calories'
nutritionix_appKey = 'd150be1ac1688616495f3dd78d40483b'
nutritionix_appId= '76284f94'
cache_dic = {}
consumer_key = 'hrOpsDPRDtmKFA5ZvtSm4KpJy'
consumer_secret = 'aXaaRF5IXtmZeWs2sEP64Uz7QExBn32YTeajftfGSuKDmZ0KCy'
access_token = '2750763448-6B1i89sT0ullMjmMk7cPOhaInZPeLVVwlFk9SvN'
access_token_secret = 'JKpprBcIWFbtrO7TxzQpnZ97ZRHdi0gEiQ7efYbJb6ETO'


def signup_view(request, username="", email="", age="", weight="", height="", gender="", workout="", diseases="", aim="", error=""):
	if request.POST:
		username = request.POST['username']
		password = request.POST['password']
		verify_password = request.POST['verify_password']
		email = request.POST['email']
		age = request.POST['age']
		weight = request.POST['weight']
		height = request.POST['height']
		gender = request.POST['gender']
		workout = request.POST.get('exercise', 'A')
		diseases = request.POST.getlist('diseases')
		aim = request.POST['aim']
		try:
			wrong = []
			err = ""
			if UsernameValidated(username, wrong) and PasswordValidated(password, verify_password, wrong) and EmailValidated(email, wrong):
				BMI = bmi(weight, height)
				BMR = bmr(weight, height, age, gender)
				TDEE = tdee(workout, BMR)
				MET = met(BMR, workout)
				COLOR = color(BMR, TDEE, MET, diseases, aim)
				user = User.objects.create_user(username=username, email=email, password=password)
				user.save()
				person = Person(user=user, workout=workout, gender=gender, diseases=diseases, aim=aim, current_weight=weight, current_height=height,
								current_bmi=BMI, current_bmr=BMR, current_tdee=TDEE, current_met=MET, color=COLOR)
				person.save()
				user.backend = "django.contrib.auth.backends.ModelBackend"
				login(request, user)
				return redirect('HUnutrition:dashboard')
			else:
				for wrongs in wrong:
					err += wrongs
				error="%(wrong)s is not correct" % {"wrong" : err}
		except ValueError:
				error="The user " + username + " already exists."
	return render(request, 'HUnutrition/signup.html', {'title' : 'HU-Nutn : Sign Up', 'username' : username, 'error' : error, 'email' : email, 'age' : age, 'weight' : weight, 'height' : height})


def index_view(request):
	if request.user and request.user.is_authenticated():
		return redirect('HUnutrition:dashboard')
	return render(request, 'HUnutrition/index.html', {'title' : 'HU-Nutn'})


@login_required
def dashboard_view(request):
	CreateCache(cache_dic, request.user)
	print cache_dic
	return render(request, 'HUnutrition/dashboard.html', {'title' : 'HU-Nutn', 'cached_memory' : cache_dic })


@login_required
def analyze_view(request):
	print 
	print "analyze: ", cache_dic
	print 
	if request.GET:
		print 
		print "received users queries"
		print 
		food = request.GET['food']
		restaurant = request.GET['restaurant']
		if food and restaurant:
			items = []
			data = nutritionix_api + food + '?' + nutritionix_api_fields + '&' + 'appKey=' + nutritionix_appKey + '&' + 'appId=' + nutritionix_appId
			page = urllib2.urlopen(data)
			page_read = page.read()
			page = json.loads(page_read)
			results = page['hits']
			for hits in results:
				if (hits['fields']['brand_name']).lower() in restaurant.lower() or restaurant.lower() in (hits['fields']['brand_name']).lower():
					items.append(hits)
			return render(request, "HUnutrition/search_result.html", {'title' : 'HU-Nutn : result', 'food' : food, 'restaurant' : restaurant,'results' : items, 'cached_memory' : cache_dic})
		elif food or restaurant:
			query = food + restaurant
			data = nutritionix_api + query + '?' + nutritionix_api_fields + '&' + 'appKey=' + nutritionix_appKey + '&' + 'appId=' + nutritionix_appId
			page = urllib2.urlopen(data)
			page_read = page.read()
			page = json.loads(page_read)
			results = page['hits']
			return render(request, "HUnutrition/search_result.html", {'title' : 'HU-Nutn : result', 'food' : food, 'restaurant' : restaurant,'results' : results, 'cached_memory' : cache_dic})
		return render(request, "HUnutrition/search_result.html", {'title' : 'HU-Nutn : result'})
	return render(request, 'HUnutrition/analyze.html', {'title' : 'HU-Nutn : Analyze Food', 'cached_memory' : cache_dic})


@login_required
def nearme_view(request):
	data_place = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?'
	"""data_location = 'https://www.googleapis.com/geolocation/v1/geolocate?key='
	page = urllib2.urlopen(data_location+google_apiKey)
	location = json.loads(page.read())
	current_latitude = location['location']['lat']
	current_longitude = location['location']['lng']
	"""
	#i have hardcoded the location of DC in the following latitude and longitude, however, longitude and latitude needs to be received from above
	page = urllib2.urlopen(data_place + 'location=38.8977,-77.0366' + '&radius=500' + '&types=restaurant|bar|food' + '&key=' + google_apiKey)
	page_read = page.read()
	page = json.loads(page_read)
	results = page['results']
	return render(request, 'HUnutrition/nearme.html', {'title' : 'HU-Nutn : Near Me', 'results' : results, 'cached_memory' : cache_dic})


@login_required
def recfood_view(request, username=""):
	if (cache_dic['color'] == 'green'):
		max_calorie = 2000
	elif (cache_dic['color'] == 'yellow'):
		max_calorie = 750
	elif (cache_dic['color'] == 'red'):
		max_calorie = 350
	data = json.dumps({"appId":nutritionix_appId,
						"appKey":nutritionix_appKey,
						"query" : "*",
						"fields":["item_name","brand_name","upc","nf_calories"],
						"filters" :{ "nf_calories" : {"from":0,"to":max_calorie}}})
	req = urllib2.Request(nutritionix_api, data, {'Content-Type': 'application/json'})
	p = urllib2.urlopen(req)
	response = p.read()
	p.close()
	page = json.loads(response)
	results = page['hits']
	return render(request, 'HUnutrition/rec_food.html', {'title' : 'HU-Nutn : Recommended Food', 'results' : results, 'cached_memory' : cache_dic})


@login_required
def mybody_view(request):
	print cache_dic
	return render(request,'HUnutrition/mybody.html', {'title' : 'HU-Nutn : My Body', 'cached_memory' : cache_dic})


@login_required
def donate_view(request, username=""):
	return render(request, 'HUnutrition/donate.html', {'title' : 'HU-Nutn : Donate', 'cached_memory' : cache_dic})


@login_required
def update_view(request):
	new_age = request.POST['age']
	new_weight = request.POST['weight']
	new_height = request.POST['height']
	new_gender = request.POST['gender']
	new_workout = request.POST.get('exercise', 'A')
	new_diseases = request.POST.getlist('diseases')
	new_aim = request.POST['aim']
	new_BMI = bmi(new_weight, new_height)
	new_BMR = bmr(new_weight, new_height, new_age, new_gender)
	new_TDEE = tdee(new_workout, new_BMR)
	new_MET = met(new_BMR, new_workout)
	new_COLOR = color(new_BMR, new_TDEE, new_MET, new_diseases, new_aim)
	u = User.objects.get(username=request.user.username)
	u.person.current_weight = new_weight
	u.person.current_age = new_age
	u.person.current_height = new_height
	u.person.gender = new_gender
	u.person.workout = new_workout
	u.person.diseases = new_diseases
	u.person.aim = new_aim
	u.person.current_bmi = new_BMI
	u.person.current_bmr = new_BMR
	u.person.current_tdee = new_TDEE
	u.person.current_met = new_MET
	u.person.color = new_COLOR
	u.person.save()
	CreateCache(cache_dic, u)
	return redirect('HUnutrition:mybody')

@login_required
def html_development_view(request):
	return render(request, 'HUnutrition/html_development_page.html', {'title' : 'HU-Nutn : html development', 'cached_memory' : cache_dic})