# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('signed_up_time', models.DateField(auto_now_add=True)),
                ('current_weight', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('current_height', models.PositiveSmallIntegerField(default=0)),
                ('current_bmi', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('current_bmr', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('current_tdee', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('current_met', models.DecimalField(default=0.0, max_digits=10, decimal_places=2)),
                ('current_age', models.PositiveSmallIntegerField(default=25)),
                ('workout', models.CharField(default=b'C', max_length=2)),
                ('gender', models.CharField(default=b'm', max_length=2)),
                ('diseases', models.TextField(default=b'none')),
                ('aim', models.CharField(default=b'C', max_length=2)),
                ('color', models.CharField(default=b'green', max_length=6)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
