from django.conf.urls import include, url

from . import views

urlpatterns = [
	url(r'^$', views.index_view, name="index"),
	url(r'^signup/$', views.signup_view, name="signup"),
	url(r'^dashboard/$', views.dashboard_view, name="dashboard"),
	url(r'^dashboard/analyze/$', views.analyze_view, name="analyze"),
	url(r'^dashboard/nearme/$', views.nearme_view, name="nearme"),
	url(r'^dashboard/recommendedfood/$', views.recfood_view, name="recfood"),
	url(r'^dashboard/mybody/$', views.mybody_view, name="mybody"),
	url(r'^dashboard/donate/$', views.donate_view, name="donate"),
	url(r'^update/$', views.update_view, name="update"),
	url(
		r'^login/$',
		'django.contrib.auth.views.login',
		name='login',
		kwargs={ 'template_name' : 'HUnutrition/login.html'}
		),
	url(
		r'^accounts/logout/$',
		'django.contrib.auth.views.logout',
		name="logout",
		kwargs={'next_page' : '/'}
		),
	url(r'^htmldevelopment$', views.html_development_view, name="htmldevelopment"),
]