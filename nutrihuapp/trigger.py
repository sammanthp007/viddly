import pafy
import ffmpy
import subprocess

url1 = "https://www.youtube.com/watch?v=clJb4zx0o1o"
url2 = "https://www.youtube.com/watch?v=6xbX5hCk9FE"
url3 = "https://www.youtube.com/watch?v=VjWfcIr8JO4"

def retMix(url1, url2, url3):		
	vid1 = pafy.new(url1)
	vid2 = pafy.new(url2)
	vid3 = pafy.new(url3)

	best1 = vid1.getbest(preftype="mp4")
	best2 = vid2.getbest(preftype="mp4")
	best3 = vid3.getbest(preftype="mp4")

	best1.download(filepath="DOTC.mp4")
	best2.download(filepath="Pretend.mp4")
	best3.download(filepath="Rooney.mp4")



	subprocess.call(["ffmpeg", "-i", "DOTC.mp4", "-vf", "scale=1280:720, setsar=1:1", "-ss", "0", "-t", "20", "s_dotc.mp4"])
	subprocess.call(["ffmpeg", "-i", "Pretend.mp4", "-vf", "scale=1280:720, setsar=1:1", "-ss", "94", "-t", "30", "s_pre.mp4"])
	subprocess.call(["ffmpeg", "-i", "Rooney.mp4", "-vf", "scale=1280:720, setsar=1:1", "-ss", "0", "-t", "30", "s_roo.mp4"])

	subprocess.call(['ffmpeg', '-i', 's_dotc.mp4', '-i', 's_roo.mp4', '-filter_complex', '[0:v:0] [0:a:0] [1:v:0] [1:a:0] concat=n=2:v=1:a=1 [v] [a]', 
	 	'-map', '[v]', '-map', '[a]', 'sub_out.mp4'])

	subprocess.call(['ffmpeg', '-i', 'sub_out.mp4', '-i', 's_pre.mp4', '-filter_complex', '[0:v:0] [0:a:0] [1:v:0] [1:a:0] concat=n=2:v=1:a=1 [v] [a]', 
	 	'-map', '[v]', '-map', '[a]', 'fin_out.mp4'])





# ffmpeg -i half_out.mp4 -vf scale=320:240,setsar=1:1 half_out2.mp4
# ffmpeg -i dil_se.mp4 -i  -vn -

# ffmpeg -i doc.mp4 -i half_out2.mp4 -filter_complex 
# "[0:v:0] [0:a:0] [1:v:0] [1:a:0] concat=n=2:v=1:a=1 [v] [a]" 
# -map "[v]" -map "[a]" main_out.mp4