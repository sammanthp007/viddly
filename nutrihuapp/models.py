from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.

class Person(models.Model):	
	def __unicode__(self):
		return self.user.username
	
	user = models.OneToOneField(settings.AUTH_USER_MODEL)

	signed_up_time = models.DateField(auto_now_add=True)
	current_weight = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
	current_height = models.PositiveSmallIntegerField(default=0)
	current_bmi = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
	current_bmr = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
	current_tdee = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
	current_met = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
	current_age = models.PositiveSmallIntegerField(default=25)
	workout = models.CharField(default='C', max_length=2)
	gender = models.CharField(default='m', max_length=2)
	diseases = models.TextField(default='none')
	aim = models.CharField(default='C', max_length=2)
	color = models.CharField(default='green', max_length=6)