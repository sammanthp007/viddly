import re

def PasswordValidated(password, verify_password, wrong):
	if (password == verify_password) and (re.match(r'[A-Za-z0-9!@#$%^&+_=-]{5,20}', password)):
		return True
	wrong.append("password")
	return False

def UsernameValidated(username, wrong):
	if re.match(r'^[A-Za-z0-9_.-]{3,30}$', username):
		return True
	wrong.append("username")
	return False

def EmailValidated(email, wrong):
	if re.match(r'^[A-Za-z0-9._-]+@[A-Za-z0-9.]+$', email):
		return True
	wrong.append("email")
	return False

def make_decimal(num):
	return int((num * 100) + 0.5) / 100

def bmi(weight, height):
	return make_decimal((float(weight) * 703) / (float(height) * float(height)))

def bmr(weight, height, age, gender):
	if gender=='f':
		return make_decimal(655 + (4.35 * float(weight)) + (4.7 * float(height)) - (4.7 * float(age)))
	elif gender =='m':
		return make_decimal(66 + (6.23 * float(weight)) + (12.7 * float(height)) - (6.8 * float(age)))

def tdee(workout, bmr):
	if workout=='A':
		hbf = 1.9
	elif workout == 'B':
		hbf = 1.725
	elif workout == 'C':
		hbf = 1.55
	elif workout == 'D':
		hbf = 1.375
	elif workout == 'E':
		hbf = 1.2
	return make_decimal(float(bmr) * hbf)

def met(bmr, workout):
	#i have assumed an average of 5 METS for every activity: formula - MET * (BMR/24) * duration
	#sleeping: for 7 hours a day
	sleep = (0.9) * (bmr / 24) * 7
	#sit idly for 16 hours
	sit_idle = (1.0) * (bmr / 24) * 7
	##walk for 1 hour
	walk = (2.9) * (bmr / 24) * 1
	return sleep + sit_idle + walk

#in determining the color, i have assumed that a person just satisfies his need/tdee
def color(BMR, TDEE, MET, diseases, aim):
	if int(MET) < int(TDEE):
		return 'red'
	elif int(MET) == int(TDEE):
		print "color is yellow"
		return 'yellow'
	elif int(MET) > int(TDEE):
		print "color is green"
		return 'green'

def CreateCache(dic, usr):
	dic['username'] = str(usr.username)
	dic['email'] = str(usr.email)
	dic['age'] = str(usr.person.current_age)
	dic['weight'] = str(usr.person.current_weight)
	dic['height'] = str(usr.person.current_height)
	dic['bmi'] = str(usr.person.current_bmi)
	dic['bmr'] = str(usr.person.current_bmr)
	dic['tdee'] = str(usr.person.current_tdee)
	dic['met'] = str(usr.person.current_met)
	dic['workout'] = str(usr.person.workout)
	dic['gender'] = str(usr.person.gender)
	dic['diseases'] = str(usr.person.diseases)
	dic['aim'] = str(usr.person.aim)
	dic['color'] = str(usr.person.color)